package org.ml4se.kaosbot.service.rule;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ml4se.kaosbot.model.Goal;
import org.ml4se.kaosbot.model.GoalWithAgent;
import org.ml4se.kaosbot.model.MainGoal;
import org.ml4se.kaosbot.util.PropertiesUtil;

import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.Tree;

public class GoalWithAgentRuleTest {
	
	private static StanfordCoreNLP pipeline;
	private static GoalWithAgentRule goalWithAgentRule;
	
	@BeforeClass
	public static void init() {
		pipeline = new StanfordCoreNLP(PropertiesUtil.props);
		goalWithAgentRule = new GoalWithAgentRule();
	}
	
	
	private Tree parseTree(String sentence) {
		CoreDocument document = new CoreDocument(sentence);
		pipeline.annotate(document);

		CoreSentence coreSentence = document.sentences().get(0);
		return coreSentence.constituencyParse();
	}
	
	@Test
	public void deveCriarUmGoalWithAgent() {
		String sentence = "The system should validate data";
		Tree tree = this.parseTree(sentence);
		
		Goal goal = goalWithAgentRule.execute(tree, new MainGoal());
		Assert.assertThat(goal, CoreMatchers.instanceOf(GoalWithAgent.class));
		GoalWithAgent goalWithAgent = (GoalWithAgent) goal;
		Assert.assertNotNull(goalWithAgent.getAgent());
		Assert.assertEquals("system", goalWithAgent.getAgent().getName());
		Assert.assertEquals("validate data", goalWithAgent.getDescription());
	}
}
