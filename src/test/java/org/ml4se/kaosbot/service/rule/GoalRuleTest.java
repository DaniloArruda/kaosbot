package org.ml4se.kaosbot.service.rule;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ml4se.kaosbot.model.Goal;
import org.ml4se.kaosbot.model.MainGoal;
import org.ml4se.kaosbot.util.GoalTestUtil;
import org.ml4se.kaosbot.util.PropertiesUtil;

import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.Tree;

public class GoalRuleTest {
	
	private static StanfordCoreNLP pipeline;
	private static GoalRule goalRule;
	
	@BeforeClass
	public static void init() {
		pipeline = new StanfordCoreNLP(PropertiesUtil.props);
		goalRule = new GoalRule();
	}
	
	
	private Tree parseTree(String sentence) {
		CoreDocument document = new CoreDocument(sentence);
		pipeline.annotate(document);

		CoreSentence coreSentence = document.sentences().get(0);
		return coreSentence.constituencyParse();
	}

	@Test
	public void naoDeveCriarUmGoalPqTemUmSujeito() {
		String sentence = "The system must detect the passage of the card";
		Tree tree = this.parseTree(sentence);
		
		Goal goal = goalRule.execute(tree, new MainGoal());
		Assert.assertNull(goal);
	}
	
	@Test
	public void deveCriarUmGoal() {
		List<GoalTestUtil> listGoalsTest = Arrays.asList(
				//new GoalTestUtil("detect the passage of the card", "detect the passage of the card"),
				//new GoalTestUtil("reduce cost of meter reading", "reduce cost of meter reading"),
				new GoalTestUtil("To help therapists on data collection when applying ABA Therapy on autist students", "help therapists on data collection"));
		
		for (GoalTestUtil goalTest : listGoalsTest) {
			this.criarGoalTeste(goalTest);
		}
	}
	
	@Test
	public void deveCriarUmGoalComAF3DeAlfonso() {
		String sentence = "Ensure that the manager saves customer information";
		Tree tree = this.parseTree(sentence);
		
		Goal goal = goalRule.execute(tree, new MainGoal());
		Assert.assertNotNull(goal);
		Assert.assertEquals("save customer information", goal.getDescription());
	}
	
	
	private void criarGoalTeste(GoalTestUtil goalTest) {
		Tree tree = this.parseTree(goalTest.sentence);
		
		Goal goal = goalRule.execute(tree, new MainGoal());
		Assert.assertNotNull(goal);
		Assert.assertEquals(goalTest.description, goal.getDescription());
	}
}
