package org.ml4se.kaosbot.service;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ml4se.kaosbot.model.Goal;
import org.ml4se.kaosbot.model.GoalWithAgent;
import org.ml4se.kaosbot.model.MainGoal;
import org.ml4se.kaosbot.service.rule.MainGoalRule;
import org.ml4se.kaosbot.util.GoalTestUtil;
import org.ml4se.kaosbot.util.PropertiesUtil;

import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.Tree;

public class IdentifierGoalTypesTest {
	
	private static StanfordCoreNLP pipeline;
	private static MainGoalRule mainGoalRule;
	
	@BeforeClass
	public static void init() {
		pipeline = new StanfordCoreNLP(PropertiesUtil.props);
		mainGoalRule = new MainGoalRule();
	}
	
	private Tree parseTree(String sentence) {
		CoreDocument document = new CoreDocument(sentence);
		pipeline.annotate(document);

		CoreSentence coreSentence = document.sentences().get(0);
		return coreSentence.constituencyParse();
	}

	@Test
	public void deveCriarUmMainGoal() {
		String sentence = "The system should validate data";
		mainGoalRule = new MainGoalRule();
		Tree tree = this.parseTree(sentence);
		MainGoal goal = mainGoalRule.execute(tree, new MainGoal());
		Assert.assertNotNull(goal);
		Assert.assertEquals("validate data", goal.getDescription());
		Assert.assertEquals("system", goal.getOwner());
	}
	
	@Test
	public void naoDeveCriarAgenteQuandoFalaDoSujeitoDoMainGoal() {
		IdentifierGoalTypes identifierGoalTypes = IdentifierGoalTypes.getInstance();
		
		GoalTestUtil goalTest = new GoalTestUtil("The system must be secure.", "be secure");
		MainGoal mainGoal = identifierGoalTypes.extractMainGoal(goalTest.sentence);
		Assert.assertNotNull(mainGoal);
		Assert.assertEquals(goalTest.description, mainGoal.getDescription());
		
		goalTest = new GoalTestUtil("The system shall be reliable.", "be reliable");
		Goal goal = identifierGoalTypes.identify(goalTest.sentence, mainGoal);
		Assert.assertNotNull(goal);
		Assert.assertThat(goal, CoreMatchers.not(CoreMatchers.instanceOf(GoalWithAgent.class)));
		Assert.assertEquals(goalTest.description, goal.getDescription());
		
	}
}
