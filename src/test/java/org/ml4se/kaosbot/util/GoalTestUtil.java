package org.ml4se.kaosbot.util;

public class GoalTestUtil {
	public GoalTestUtil(String sentence, String description) {
		this.sentence = sentence;
		this.description = description;
	}
	public String sentence;
	public String description;
}
