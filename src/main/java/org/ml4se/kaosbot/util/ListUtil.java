package org.ml4se.kaosbot.util;

import java.util.List;

@SuppressWarnings("rawtypes")
public class ListUtil {
	
	public static boolean notEmpty(List list) {
		return list != null && !list.isEmpty();
	}
	
	public static boolean empty(List list) {
		return !notEmpty(list);
	}
}