package org.ml4se.kaosbot.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.trees.Tree;

public class ParseTreeUtil {
	public static List<String> VERBS = Arrays.asList("VB", "VBD", "VBG", "VBN", "VBP", "VBZ");
	public static List<String> NOUNS = Arrays.asList("NN", "NNP", "NNPS", "NNS");
	public static List<String> OBJECTS = Arrays.asList("NP", "PP", "ADJP");
	
	public static List<Tree> getPreTerminalsVerbs(Tree tree) {
		return getPreTerminalsIn(tree, VERBS);
	}
	
	public static List<Tree> getPreTerminalsNouns(Tree tree) {
		return getPreTerminalsIn(tree, NOUNS);
	}

	public static List<Tree> getPreTerminalsIn(Tree tree, List<String> categories) {
		List<Tree> preTerminalOfCategory = new ArrayList<>();
		
		if (tree.isPreTerminal() && categories.contains(tree.value())) {
			preTerminalOfCategory.add(tree);
			return preTerminalOfCategory;
		}
		
		for (Tree child : tree.children()) {
			preTerminalOfCategory = getPreTerminalsInExecute(child, categories, preTerminalOfCategory);
			if (ListUtil.notEmpty(preTerminalOfCategory))
				break;
		}
		
		return preTerminalOfCategory;
	}

	private static List<Tree> getPreTerminalsInExecute(Tree tree, List<String> categories, List<Tree> preTerminalOfCategory) {
		
		if (tree.isPreTerminal()) {
			if (categories.contains(tree.value())) 
				preTerminalOfCategory.add(tree);
				
			return preTerminalOfCategory;
		} 
		

		for (Tree child : tree.children()) {
			preTerminalOfCategory = getPreTerminalsInExecute(child, categories, preTerminalOfCategory);
		}
		
		return preTerminalOfCategory;
	}
	
	public static List<Tree> extractSubject(Tree sentenceTree) {
		List<Tree> subject = null;
		
		Tree firstChild = sentenceTree.firstChild();
		if (firstChild.value().equals("NP")) {
			subject = getPreTerminalsNouns(firstChild);
		}
		
		return subject;
	}
	
	public static List<Tree> extractPredicate(Tree sentenceTree) {
		List<Tree> predicate = null;
		
		Tree treeVP = getChildTreeVP(sentenceTree);
		
		Tree treeSBAR = getChildTree(treeVP, "SBAR");
		if (treeSBAR != null) {
			sentenceTree = getSubTreeS(treeSBAR);
			treeVP = getChildTreeVP(sentenceTree);
		}
		
		for (Tree child : treeVP.children()) {
			if (!child.value().equals("S")) {
				predicate = getPreTerminalsVerbs(child);
				if (predicate != null && !predicate.isEmpty()) {
					return predicate;
				}
			}
		}
		
		return predicate;
	}

	public static List<Tree> extractObject(Tree treeVP) {
		List<Tree> object = new ArrayList<>();
	
		for (Tree child : treeVP.children()) {
			if (OBJECTS.contains(child.value())) {
				object.addAll(child.getLeaves());
			} 
//			else {
//				object.addAll(extractObject(child));
//			}
		}
		
		return object;
	}

	public static Tree getSubTreeS(Tree tree) {
		return getSubtree(tree, "S");
	}

	public static Tree getChildTreeVP(Tree sentenceTree) {
		return getChildTree(sentenceTree, "VP");
	}
	
	public static Tree getChildTree(Tree tree, String label) {
		for (Tree subtree : tree.children()) {
			if (subtree.value().equals(label))
				return subtree;
		}
		
		return null;
	}
	
	public static Tree getSubtree(Tree tree, String label) {
		Tree subtree = null;
		for (Tree child : tree.children()) {
			if (child.value().equals(label)) {
				subtree = child;
				break;
			} else
				subtree = getSubtree(child, label);
		}
		
		return subtree;
	}
	
	public static List<Tree> getLeafs(List<Tree> tree) {
		List<Tree> leafs = new ArrayList<>();
		
		for (Tree child : tree) {
			leafs.addAll(child.getLeaves());
		}
		
		return leafs;
	}
	
	public static boolean hasSubject(Tree sentenceTree) {
		List<Tree> subject = extractSubject(sentenceTree);
		return subject != null && !subject.isEmpty();
	}
	
	public static String createDescription(List<Tree> predicate, List<Tree> object) {
		String description = "";
		
		for (Tree t : predicate) {
			description += t.value() + " ";
		}
		
		if (object != null) {
			for (Tree t : object) {
				description += t.value() + " ";
			}
		}
		
		return description.trim();
	}
	
	public static String createAgentName(List<Tree> subject) {
		String name = "";
		
		for (Tree t : subject) {
			name += t.value() + " ";
		}
		
		return name.trim();
	}
	
	public static boolean hasModal(Tree node) {
		BooleanUtil retorno = new BooleanUtil();
		retorno.value = false;

		hasModalExecute(node, retorno);

		return retorno.value;
	}

	public static void hasModalExecute(Tree node, BooleanUtil indicador) {
		
		if (node.value().equals("MD")) {
			indicador.value = true;
		}

		if (node.children() != null) {
			for (Tree child : node.children()) {
				hasModalExecute(child, indicador);
			}
		}
	}
	
	public static List<Tree> getLeafsLemmatizedVerbs(List<Tree> tree) {
		List<Tree> leafs = getLeafs(tree);
		
		for (int i = 0; i < tree.size(); i++) {
			Tree leaf = leafs.get(i);
			Tree node = tree.get(i);
			String value = leaf.value();
			String label = node.label().toString();
			String s = Morphology.lemmaStatic(value, label, true);
			leaf.setValue(s);
		}
		
		return leafs;
	}
}
