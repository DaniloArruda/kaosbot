package org.ml4se.kaosbot.util;

public class ManagerGoalsUtil {
	
	private static Integer COUNTER_GOALS = 1;
	private static Integer COUNTER_AGENTS = 1;
	
	public static Integer autoIncrementGoals() {
		return COUNTER_GOALS++;
	}
	
	public static Integer autoIncrementAgents() {
		return COUNTER_AGENTS++;
	}
}
