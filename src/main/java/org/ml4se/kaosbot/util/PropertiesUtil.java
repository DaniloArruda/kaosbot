package org.ml4se.kaosbot.util;

import java.util.Properties;

public class PropertiesUtil {
	
	public static Properties props;
	
	static {
		props = new Properties();
		props.setProperty("annotators", "tokenize,ssplit,pos,parse,lemma");
	}
}
