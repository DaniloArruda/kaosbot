package org.ml4se.kaosbot;

import org.ml4se.kaosbot.bot.Bot;
import org.ml4se.kaosbot.bot.BotConsole;
import org.ml4se.kaosbot.model.Agent;
import org.ml4se.kaosbot.model.Expectative;
import org.ml4se.kaosbot.model.Goal;
import org.ml4se.kaosbot.model.GoalWithAgent;
import org.ml4se.kaosbot.model.MainGoal;
import org.ml4se.kaosbot.model.Requirement;
import org.ml4se.kaosbot.service.IdentifierGoalTypes;

public class App {
	
	private Bot bot = new BotConsole();
	private IdentifierGoalTypes identifier = IdentifierGoalTypes.getInstance();
	private MainGoal mainGoal = null;
	
	public static void main(String[] args) {
		new App().start();
	}
	
	public void start() {
		String response = bot.ask("What is your goal?");
		mainGoal = this.extractMainGoal(response);
		
		response = bot.ask("What is need to " + mainGoal.getDescription() + " ?");
		identifyRefinements(mainGoal, response);
	}
	
	public void identifyRefinements(Goal goal, String response) {
		Goal auxGoal;
		
		if (response.equalsIgnoreCase("anything") && !response.equalsIgnoreCase("nothing"))
			return;
		
		auxGoal = this.extractGoal(response);
		
		if (auxGoal instanceof GoalWithAgent) {
			GoalWithAgent goalWithAgent = (GoalWithAgent) auxGoal;
			Agent agent = goalWithAgent.getAgent();
			response = bot.ask("Is " + agent.getName() + " a Environment Agent?");
			
			if (response.equalsIgnoreCase("yes")) {
				Expectative expectative = Expectative.fromGoalWithAgent(goalWithAgent);
				auxGoal = expectative;
			} else {
				bot.talk("Then it is a Software Agent.");
				Requirement requirement = Requirement.fromGoalWithAgent(goalWithAgent);
				auxGoal = requirement;
			}
			
			goal.getSubgoals().add(auxGoal);
			auxGoal.setParent(goal);
		} else {
			goal.getSubgoals().add(auxGoal);
			auxGoal.setParent(goal);
			
			response = bot.ask("And what is need to " + auxGoal.getDescription() + " ?");
			identifyRefinements(auxGoal, response);
			
			response = bot.ask("What else is need to " + goal.getDescription() + " ?");
			identifyRefinements(goal, response);
			
		}
		
	}
	
	private MainGoal extractMainGoal(String sentence) {
		MainGoal mainGoal = identifier.extractMainGoal(sentence);
		while (mainGoal == null) {
			sentence = bot.ask("Sorry! I did not understand. Could you please tell me your goal again?");
			mainGoal = identifier.extractMainGoal(sentence);
		}
		
		return mainGoal;
	}
	
	private Goal extractGoal(String sentence) {
		Goal goal = identifier.identify(sentence, mainGoal);
		while (goal == null) {
			sentence = bot.ask("Sorry! I did not understand. Could you please tell me your goal again?");
			goal = identifier.identify(sentence, mainGoal);
		}
		
		return goal;
	}
}
