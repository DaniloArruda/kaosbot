package org.ml4se.kaosbot.model;

public class MainGoal extends Goal {
	private String owner;

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
}
