package org.ml4se.kaosbot.model;

public class SoftwareAgent extends Agent {

	public static SoftwareAgent fromAgent(Agent agent) {
		SoftwareAgent softwareAgent = new SoftwareAgent();
		softwareAgent.setId(agent.getId());
		softwareAgent.setName(agent.getName());
		return softwareAgent;
	}

}
