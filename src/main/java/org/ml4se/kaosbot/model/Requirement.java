package org.ml4se.kaosbot.model;

public class Requirement extends Goal {
	private SoftwareAgent agent;
	
	public static Requirement fromGoalWithAgent(GoalWithAgent goalWithAgent) {
		Requirement requirement = new Requirement();
		SoftwareAgent agent = SoftwareAgent.fromAgent(goalWithAgent.getAgent());
		
		requirement.setId(goalWithAgent.getId());
		requirement.setDescription(goalWithAgent.getDescription());
		requirement.setAgent(agent);
		
		return requirement;
	}

	public SoftwareAgent getAgent() {
		return agent;
	}

	public void setAgent(SoftwareAgent agent) {
		this.agent = agent;
	}

	
}
