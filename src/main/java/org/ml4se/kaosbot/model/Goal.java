package org.ml4se.kaosbot.model;

import java.util.ArrayList;
import java.util.List;

public class Goal {
	protected Integer id;
	protected String description;
	protected List<Goal> subgoals = new ArrayList<Goal>();
	protected Goal parent;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Goal> getSubgoals() {
		return subgoals;
	}
	public void setSubgoals(List<Goal> subgoals) {
		this.subgoals = subgoals;
	}
	public Goal getParent() {
		return parent;
	}
	public void setParent(Goal parent) {
		this.parent = parent;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Goal other = (Goal) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
