package org.ml4se.kaosbot.model;

public class Expectative extends Goal {
	private EnvironmentAgent agent;
	
	public static Expectative fromGoalWithAgent(GoalWithAgent goalWithAgent) {
		Expectative expectative = new Expectative();
		EnvironmentAgent agent = EnvironmentAgent.fromAgent(goalWithAgent.getAgent());
		
		expectative.setId(goalWithAgent.getId());
		expectative.setDescription(goalWithAgent.getDescription());
		expectative.setAgent(agent);
		
		return expectative;
	}

	public EnvironmentAgent getAgent() {
		return agent;
	}

	public void setAgent(EnvironmentAgent agent) {
		this.agent = agent;
	}
}
