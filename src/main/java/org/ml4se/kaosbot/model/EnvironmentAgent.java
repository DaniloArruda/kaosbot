package org.ml4se.kaosbot.model;

public class EnvironmentAgent extends Agent {

	public static EnvironmentAgent fromAgent(Agent agent) {
		EnvironmentAgent environmentAgent = new EnvironmentAgent();
		environmentAgent.setId(agent.getId());
		environmentAgent.setName(agent.getName());
		return environmentAgent;
	}

}
