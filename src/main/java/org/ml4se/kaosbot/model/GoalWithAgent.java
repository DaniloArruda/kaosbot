package org.ml4se.kaosbot.model;

public class GoalWithAgent extends Goal {
	private Agent agent;

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}
}
