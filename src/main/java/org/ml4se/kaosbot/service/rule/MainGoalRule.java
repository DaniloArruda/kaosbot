package org.ml4se.kaosbot.service.rule;

import static org.ml4se.kaosbot.util.ParseTreeUtil.createDescription;
import static org.ml4se.kaosbot.util.ParseTreeUtil.extractObject;
import static org.ml4se.kaosbot.util.ParseTreeUtil.extractPredicate;
import static org.ml4se.kaosbot.util.ParseTreeUtil.extractSubject;
import static org.ml4se.kaosbot.util.ParseTreeUtil.getLeafs;
import static org.ml4se.kaosbot.util.ParseTreeUtil.getSubTreeS;

import java.util.List;

import org.ml4se.kaosbot.model.MainGoal;
import org.ml4se.kaosbot.util.ManagerGoalsUtil;
import org.ml4se.kaosbot.util.ParseTreeUtil;

import edu.stanford.nlp.trees.Tree;

public class MainGoalRule implements Rule {

	@Override
	public MainGoal execute(Tree tree, MainGoal mainGoal) {
		Tree sentenceTree = getSubTreeS(tree);
		
		if (sentenceTree != null) {
			List<Tree> subject = extractSubject(sentenceTree);
			
			List<Tree> predicate = extractPredicate(sentenceTree);
			if (predicate != null && !predicate.isEmpty()) {
				Tree treeVP = predicate.get(0).parent(tree);
				List<Tree> object = extractObject(treeVP);
				
				String description = createDescription(getLeafs(predicate), object);
				String owner = null;
				if (subject != null) {
					owner = ParseTreeUtil.createAgentName(getLeafs(subject));
				}
				
				MainGoal goal = new MainGoal();
				goal.setId(ManagerGoalsUtil.autoIncrementGoals());
				goal.setDescription(description);
				goal.setOwner(owner);
				
				return goal;
				
			}
		}
		
		return null;
	}

}
