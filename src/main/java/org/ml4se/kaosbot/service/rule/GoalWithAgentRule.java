package org.ml4se.kaosbot.service.rule;

import java.util.List;

import org.ml4se.kaosbot.model.Agent;
import org.ml4se.kaosbot.model.Goal;
import org.ml4se.kaosbot.model.GoalWithAgent;
import org.ml4se.kaosbot.model.MainGoal;
import org.ml4se.kaosbot.util.ListUtil;
import org.ml4se.kaosbot.util.ManagerGoalsUtil;
import static org.ml4se.kaosbot.util.ParseTreeUtil.*;

import edu.stanford.nlp.trees.Tree;

public class GoalWithAgentRule implements Rule {

	public Goal execute(Tree root, MainGoal mainGoal) {
		Tree sentenceTree = getSubTreeS(root);
		
		if (sentenceTree != null) {
			List<Tree> subject = extractSubject(sentenceTree);
			if (ListUtil.notEmpty(subject) && !createAgentName(getLeafs(subject)).equals(mainGoal.getOwner())) { // verifica se não é o agente principal
				
				List<Tree> predicate = extractPredicate(sentenceTree);
				if (ListUtil.notEmpty(predicate)) {
					
					Tree treeVP = predicate.get(0).parent(root);
					List<Tree> object = extractObject(treeVP);
					
					String agentName = createAgentName(getLeafs(subject));
					String descriptionGoal = createDescription(getLeafs(predicate), object);
					
					Agent agent = new Agent();
					agent.setId(ManagerGoalsUtil.autoIncrementAgents());
					agent.setName(agentName);
					
					GoalWithAgent goal = new GoalWithAgent();
					goal.setId(ManagerGoalsUtil.autoIncrementGoals());
					goal.setDescription(descriptionGoal);
					goal.setAgent(agent);
					
					return goal;
					
				}
				
			}
			
		}
		
		return null;
	}
}
