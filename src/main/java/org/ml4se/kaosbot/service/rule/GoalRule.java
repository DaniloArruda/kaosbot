package org.ml4se.kaosbot.service.rule;

import java.util.List;

import org.ml4se.kaosbot.model.Goal;
import org.ml4se.kaosbot.model.MainGoal;
import org.ml4se.kaosbot.util.ListUtil;
import org.ml4se.kaosbot.util.ManagerGoalsUtil;
import static org.ml4se.kaosbot.util.ParseTreeUtil.*;

import edu.stanford.nlp.trees.Tree;

public class GoalRule implements Rule {

	public Goal execute(Tree root, MainGoal mainGoal) {
		Tree sentenceTree = getSubTreeS(root);

		if (sentenceTree != null) {
			List<Tree> subject = extractSubject(sentenceTree);
			if (ListUtil.empty(subject) || createAgentName(getLeafs(subject)).equals(mainGoal.getOwner())) {
				List<Tree> predicate = extractPredicate(sentenceTree);
				if (ListUtil.notEmpty(predicate)) {
					Tree treeVP = predicate.get(0).parent(root);
					List<Tree> object = extractObject(treeVP);

					String description = createDescription(getLeafsLemmatizedVerbs(predicate), object);

					Goal goal = new Goal();
					goal.setId(ManagerGoalsUtil.autoIncrementGoals());
					goal.setDescription(description);
					return goal;

				}	
			}
		}

		return null;
	}

	
}
