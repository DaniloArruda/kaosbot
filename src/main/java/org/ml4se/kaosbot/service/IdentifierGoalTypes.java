package org.ml4se.kaosbot.service;

import java.util.Arrays;
import java.util.List;

import org.ml4se.kaosbot.model.Goal;
import org.ml4se.kaosbot.model.MainGoal;
import org.ml4se.kaosbot.service.rule.GoalRule;
import org.ml4se.kaosbot.service.rule.GoalWithAgentRule;
import org.ml4se.kaosbot.service.rule.MainGoalRule;
import org.ml4se.kaosbot.service.rule.Rule;
import org.ml4se.kaosbot.util.PropertiesUtil;

import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.Tree;

public class IdentifierGoalTypes {
	private static IdentifierGoalTypes instance = null;
	
	private List<Rule> rules;
	private StanfordCoreNLP pipeline;

	private IdentifierGoalTypes() {
		this.rules = Arrays.asList(new GoalRule(), new GoalWithAgentRule());
		this.pipeline = new StanfordCoreNLP(PropertiesUtil.props);
	}
	
	private IdentifierGoalTypes(Rule ...rules) {
		this.rules = Arrays.asList(rules);
		this.pipeline = new StanfordCoreNLP(PropertiesUtil.props);
	}
	
	public static IdentifierGoalTypes getInstance() {
		if (instance == null) {
			instance = new IdentifierGoalTypes();
		}
		return instance;
	}
	
	public static IdentifierGoalTypes getInstance(Rule ...rules) {
		if (instance == null) {
			instance = new IdentifierGoalTypes(rules);
		} else {
			instance.rules = Arrays.asList(rules);
		}
		return instance;
	}

	public Goal identify(String sentence, MainGoal mainGoal) {
		CoreDocument document = new CoreDocument(sentence);
		pipeline.annotate(document);

		CoreSentence coreSentence = document.sentences().get(0);
		Tree tree = coreSentence.constituencyParse();
		
		for (Rule rule : this.rules) {
			Goal goal = rule.execute(tree, mainGoal);
			if (goal != null) {
				return goal;
			}
		}

		return null;
	}


	public MainGoal extractMainGoal(String sentence) {
		CoreDocument document = new CoreDocument(sentence);
		pipeline.annotate(document);

		CoreSentence coreSentence = document.sentences().get(0);
		Tree tree = coreSentence.constituencyParse();
		
		MainGoalRule mainGoalRule = new MainGoalRule();
		return mainGoalRule.execute(tree, new MainGoal());
	}
}
