package org.ml4se.kaosbot.bot;

import java.util.Scanner;

public class BotConsole implements Bot {

	private Scanner scanner = new Scanner(System.in);
	
	public String ask(String question) {
		talk(question);
		return scanner.nextLine();
	}

	@Override
	public void talk(String string) {
		System.out.println(string);
	}

}
