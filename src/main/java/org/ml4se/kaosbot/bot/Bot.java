package org.ml4se.kaosbot.bot;

public interface Bot {
	public String ask(String question);
	public void talk(String string);
}
